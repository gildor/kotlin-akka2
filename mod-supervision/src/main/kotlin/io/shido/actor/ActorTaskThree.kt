package io.shido.actor

import akka.actor.AbstractLoggingActor
import akka.actor.Props
import akka.japi.pf.ReceiveBuilder

/**
 * @author Vadim Vera
 */
class ActorTaskThree : AbstractLoggingActor() {
  companion object {
    fun props(): Props {
      return Props.create(ActorTaskThree::class.java, { ActorTaskThree() })
    }
  }

  override fun createReceive(): Receive {
    return ReceiveBuilder.create()
        .match(MsgTaskThree::class.java, { process(it) })
        .matchAny { process(it) }
        .build()
  }

  //===============================================================================================
  //
  //===============================================================================================

  private fun process(message: MsgTaskThree) {
    log().debug("Processing message: $message")
    log().info("All done!")
  }

  private fun process(message: Any) {
    log().warning("Received unexpected message: $message")
    unhandled(message)
  }
}
