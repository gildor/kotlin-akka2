package io.shido.actor

import akka.actor.AbstractActor
import akka.actor.AbstractLoggingActor
import akka.actor.OneForOneStrategy
import akka.actor.Props
import akka.actor.SupervisorStrategy
import akka.japi.pf.DeciderBuilder
import akka.japi.pf.ReceiveBuilder
import scala.concurrent.duration.Duration
import java.util.UUID
import java.util.concurrent.TimeUnit

/**
 * @author Vadim Vera
 */
class Supervisor : AbstractLoggingActor() {
  private val decider = DeciderBuilder.matchAny { SupervisorStrategy.restart() }.build()

  companion object {
    fun props(): Props {
      return Props.create(Supervisor::class.java, { Supervisor() })
    }
  }

  override fun createReceive(): AbstractActor.Receive {
    return ReceiveBuilder.create()
        .match(MsgTaskOne::class.java, { this.process(it) })
        .matchAny { process(it) }
        .build()
  }

  override fun supervisorStrategy(): SupervisorStrategy {
    return OneForOneStrategy(3, Duration.create(5, TimeUnit.SECONDS), false, decider)
  }

  //===============================================================================================
  //
  //===============================================================================================

  private fun process(message: MsgTaskOne) {
    log().debug("Processing message [$message]...")
    val actor = context().actorOf(ActorTaskTwo.props()) // Notice an actor per request is created
    actor.tell(MsgTaskTwo(message.firstName, UUID.randomUUID().toString()), self())
  }

  private fun process(message: Any) {
    log().warning("Received unexpected message: $message")
    unhandled(message)
  }
}
