package io.shido.actor

import java.io.Serializable
import java.util.UUID

/**
 * @author Vadim Vera
 */
data class MsgTaskOne(val firstName: String) : Serializable

data class MsgTaskTwo(val firstName: String, val lastName: String) : Serializable

data class MsgTaskThree(val id: UUID, val firstName: String, val lastName: String) : Serializable
