package io.shido.actor

import akka.actor.AbstractLoggingActor
import akka.actor.ActorRef
import akka.actor.Props
import akka.japi.pf.ReceiveBuilder
import java.util.Optional
import java.util.UUID

/**
 * @author Vadim Vera
 */
class ActorTaskTwo : AbstractLoggingActor() {
  private val actor: ActorRef

  init {
    actor = context().actorOf(ActorTaskThree.props())
  }

  companion object {
    fun props(): Props {
      return Props.create(ActorTaskTwo::class.java, { ActorTaskTwo() })
    }
  }

  override fun createReceive(): Receive {
    return ReceiveBuilder.create()
        .match(MsgTaskTwo::class.java, { process(it) })
        .matchAny { process(it) }
        .build()
  }

  @Throws(Exception::class)
  override fun preRestart(reason: Throwable, message: Optional<Any>) {
    log().warning("Something failed with ${message.get()}! Its supervisor is getting notified while we're on it...")
    self().tell(message.get(), sender())
    super.preRestart(reason, message)
  }

  //===============================================================================================
  //
  //===============================================================================================

  private fun process(message: MsgTaskTwo) {
    log().debug("Processing message: $message")
    // TODO: Implement backoff with restart strategy
    //if (StrictMath.signum(StrictMath.random() /*- StrictMath.random()*/) > 0) { // Fails requests based on bits' luck
    //  log().warning("It considered your request...but the chips were weak! Dropping message: {}", message);
    //  throw new Exception(String.format("Couldn't process the message: %s", message));
    //}
    actor.tell(MsgTaskThree(UUID.randomUUID(), message.firstName, message.lastName), self())
  }

  private fun process(message: Any) {
    log().warning("Received unexpected message: $message")
    unhandled(message)
  }
}
