package io.shido

import akka.actor.ActorSystem
import io.shido.actor.MsgTaskOne
import io.shido.actor.Supervisor
import mu.KotlinLogging
import scala.concurrent.duration.Duration
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicLong

/**
 * @author Vadim Vera
 */
@Throws(Exception::class)
fun main(args: Array<String>) {
  val logger = KotlinLogging.logger { }
  val system = ActorSystem.create("actor-system")
  logger.info("Starting actor system...")
  val actor = system.actorOf(Supervisor.props(), "supervisor")
  val counter = AtomicLong()
  val interval = Duration.create(3, TimeUnit.SECONDS)
  val delay = Duration.Zero()
  system.scheduler()
      .schedule(delay, interval, actor, MsgTaskOne("request-${counter.incrementAndGet()}"), system.dispatcher(), null)
  logger.info("We are processing everything in intervals of ${interval.toSeconds()} second(s)")
  logger.info("Press RETURN to finish at any time...")
  System.`in`.read()
  system.terminate()
}
