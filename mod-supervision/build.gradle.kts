import org.jetbrains.kotlin.gradle.dsl.Coroutines
import org.jetbrains.kotlin.gradle.dsl.KotlinProjectExtension

plugins {
  id("com.github.johnrengelman.shadow")
  id("org.gradle.application")
  id("org.gradle.idea")
  id("org.jetbrains.kotlin.jvm")
}

description = "Akka2 - Supervision Strategies"

dependencies {
  val akka_version: String by project
  val junit_version: String by project

  // [start] Experimental
  compile("ch.qos.logback:logback-classic:1.2.3")
  // [end] Experimental

  compile("com.typesafe.akka:akka-actor_2.12:$akka_version")
  compile("com.typesafe.akka:akka-cluster_2.12:$akka_version")
  compile("com.typesafe.akka:akka-slf4j_2.12:$akka_version")
  compile("io.github.microutils:kotlin-logging:1.5.4")
  compile("org.jetbrains.kotlin:kotlin-reflect")
  compile("org.jetbrains.kotlin:kotlin-stdlib")

  testRuntime("org.junit.jupiter:junit-jupiter-engine:$junit_version")

  testCompile("com.typesafe.akka:akka-testkit_2.12:$akka_version")
  testCompile("org.assertj:assertj-core:3.10.0")
  testCompile("org.junit.jupiter:junit-jupiter-api:$junit_version")
}

//=================================================================================================
//  P L U G I N S
//=================================================================================================

configure<ApplicationPluginConvention> {
  mainClassName = "io.shido.ApplicationKt"
}

configure<KotlinProjectExtension> {
  experimental.coroutines = Coroutines.WARN
}
