package io.shido.actor

import akka.actor.ActorSystem
import akka.actor.InvalidMessageException
import akka.testkit.TestKit
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import scala.concurrent.duration.Duration

/**
 * Test suite for [Supervisor].
 *
 * @author Vadim Vera
 */
@DisplayName("Supervisor should...")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class SupervisorTest {
  @AfterAll
  fun tearDown() {
    TestKit.shutdownActorSystem(system, Duration.Inf(), true)
  }

  @Test
  @DisplayName("Handle null messages")
  fun `handle null messages`() {
    val testKit = TestKit(system)
    val actorRef = system.actorOf(Supervisor.props(3))
    val probe = testKit.testActor()
    Assertions.assertThatThrownBy { actorRef.tell(null, probe) }.isInstanceOf(InvalidMessageException::class.java)
  }

  private var system = ActorSystem.create("actor-system")
}
