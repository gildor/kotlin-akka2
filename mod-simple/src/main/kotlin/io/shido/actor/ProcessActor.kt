package io.shido.actor

import akka.Done
import akka.actor.AbstractLoggingActor
import akka.actor.Props
import akka.japi.pf.ReceiveBuilder

/**
 * @author Vadim Vera
 */
class ProcessActor : AbstractLoggingActor() {
  companion object {
    fun props(): Props {
      return Props.create(ProcessActor::class.java)
    }
  }

  override fun createReceive(): Receive {
    return ReceiveBuilder.create()
        .match(MsgContinue::class.java, { process(it) })
        .matchAny { process(it) }
        .build()
  }

  //===============================================================================================
  //
  //===============================================================================================

  private fun process(message: Any) {
    log().warning("Received unexpected message: $message")
    unhandled(message)
  }

  private fun process(message: MsgContinue) {
    log().debug("Processing message: $message")
    sender().tell(Done.getInstance(), self())
  }
}
