package io.shido.actor

import akka.Done
import akka.actor.AbstractActor
import akka.actor.AbstractLoggingActor
import akka.actor.Props
import akka.actor.ReceiveTimeout
import akka.japi.pf.ReceiveBuilder
import scala.concurrent.duration.Duration
import java.util.concurrent.TimeUnit

/**
 * @author Vadim Vera
 */
class Supervisor(timeout: Long) : AbstractLoggingActor() {
  init {
    context().setReceiveTimeout(Duration.create(timeout, TimeUnit.SECONDS)) // Sets the overall timeout execution
  }

  companion object {
    fun props(timeout: Long): Props {
      return Props.create(Supervisor::class.java, { Supervisor(timeout) })
    }
  }

  override fun createReceive(): AbstractActor.Receive {
    return ReceiveBuilder.create()
        .match(Done::class.java, { this.process(it) })
        .match(MsgStart::class.java, { this.process(it) })
        .match(ReceiveTimeout::class.java, { this.process(it) })
        .matchAny { process(it) }
        .build()
  }

  //===============================================================================================
  //
  //===============================================================================================

  private fun process(message: Any) {
    log().warning("Received unexpected message: $message")
    unhandled(message)
  }

  private fun process(message: Done) {
    log().debug("Processing message [$message]...")
    log().info("Actor ${sender().path().name()} is done!")
  }

  private fun process(message: MsgStart) {
    log().debug("Processing message [$message]...")
    message.values.forEach {
      val actor = context().actorOf(ProcessActor.props())
      actor.tell(MsgContinue(it), self())
    }
  }

  private fun process(message: ReceiveTimeout) {
    log().debug("Processing message [$message]...")
    context().system().terminate()
  }
}
