package io.shido.actor

import java.io.Serializable

/**
 * @author Vadim Vera
 */
data class MsgContinue(val value: String) : Serializable

data class MsgStart(val values: List<String>) : Serializable
