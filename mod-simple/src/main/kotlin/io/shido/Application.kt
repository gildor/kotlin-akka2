package io.shido

import akka.actor.ActorRef
import akka.actor.ActorSystem
import io.shido.actor.MsgStart
import io.shido.actor.Supervisor
import mu.KotlinLogging
import scala.concurrent.Await
import scala.concurrent.duration.Duration
import java.util.concurrent.TimeUnit

/**
 * @author Vadim Vera
 */
@Throws(Exception::class)
fun main(args: Array<String>) {
  val logger = KotlinLogging.logger { }
  val system = ActorSystem.create("actor-system")
  logger.info("Starting actor system...")
  val actor = system.actorOf(Supervisor.props(3), "supervisor")
  system.scheduler().schedule(Duration.Zero(), Duration.create(5, TimeUnit.SECONDS), Thread {
    actor.tell(MsgStart(listOf("Joe Armstrong", "Mike Williams", "Robert Virding")), ActorRef.noSender())
  }, system.dispatcher())
  Runtime.getRuntime().addShutdownHook(Thread {
    logger.warn("Shutting down application...")
    system.terminate()
    logger.info("Application stopped successfully! Enjoy the elevator music while we're offline...")
    Await.result(system.whenTerminated(), Duration.create(3, TimeUnit.SECONDS))
  })
}
