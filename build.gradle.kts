import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar
import com.github.jengelman.gradle.plugins.shadow.transformers.AppendingTransformer
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

import java.time.ZoneOffset
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

plugins {
  id("com.github.johnrengelman.shadow") version "2.0.4" apply false
  id("org.gradle.base")
  id("org.jetbrains.kotlin.jvm") version "1.2.51" apply false
}

//allprojects { }

subprojects {
  group = "io.shido"
  version = "0.1.0-SNAPSHOT"

  configurations {
    //all { exclude(module = "spring-boot-starter-tomcat") }
  }

  repositories {
    jcenter()
    mavenCentral()
  }

  //===============================================================================================
  //  T A S K S
  //===============================================================================================

  tasks.withType<KotlinCompile> {
    val javaVersion = JavaVersion.VERSION_1_8.toString()
    sourceCompatibility = javaVersion
    targetCompatibility = javaVersion
    kotlinOptions {
      //apiVersion = "1.1"
      freeCompilerArgs = listOf("-Xjsr305=strict")
      jvmTarget = javaVersion
      //languageVersion = "1.1"
    }
  }

  tasks.withType<ShadowJar> {
    baseName = project.name
    classifier = "shadow" // fat, shadow
    manifest {
      attributes.apply {
        put("Application-Name", project.name)
        put("Build-Date", ZonedDateTime.now(ZoneOffset.UTC).format(DateTimeFormatter.ISO_DATE_TIME))
        //put("Build-Number", )
        put("Created-By", System.getProperty("user.name"))
        put("Gradle-Version", gradle.gradleVersion)
        put("Implementation-Version", "${project.version}")
        put("JDK-Version", System.getProperty("java.version"))
        put("Main-Class", "io.shido.ApplicationKt")
      }
    }
    transform(AppendingTransformer::class.java, {
      resource = "reference.conf"
    })
    //mergeServiceFiles { include("META-INF/services/io.vertx.core.spi.VerticleFactory") }
    version = "${project.version}"
  }

  tasks.withType<Test> {
    useJUnitPlatform()
  }
}
