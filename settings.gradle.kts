include("mod-simple")
include("mod-supervision")

if (!JavaVersion.current().isJava8Compatible) {
  throw IllegalStateException("""This project needs Java SDK 8.x and you are using something else...
                               | Refresh and try again.""".trimMargin())
}
